function getSplitName(personName) {
  if (typeof personName === 'string') {
    // menghapus spasi duplicate pada string dan juga menghapus spasi di kedua sisi string
    const removeSpace = personName.replace(/\s+/g, ' ').trim();
    if (removeSpace.length <= 0) {
      return 'String is Empty';
    }
    // pemisahan pada string removeSpace berdasarkan spasi, membagi string tersebut menjadi array
    const name = removeSpace.split(' ');
    if (name.length <= 3) {
      if (name.length <= 2) {
        return {
          firstName: name[0],
          middleName: null,
          // kalau tipe data pada array kedua sama dengan undefined, nilai kembalian akan null,
          // kalau tidak undefined maka akan mengembalikan nilai array kedua dari name
          lastName: typeof name[1] === 'undefined' ? null : name[1],
        };
      } else
        return {
          firstName: name[0],
          middleName: name[1],
          lastName: name[2],
        };
    } else {
      return 'This function is only for 3 character name';
    }
  } else {
    return 'Error : invalid data type';
  }
}

console.log(getSplitName('Aldi Daniela Pranata'));
console.log(getSplitName('Dwi Kuncoro'));
console.log(getSplitName('Aurora'));
console.log(getSplitName('Aurora Aurelya Sukma Darma'));
console.log(getSplitName(0));
console.log(getSplitName(''));
