function getAngkaTerbesarKedua(dataNumbers) {
  if (Array.isArray(dataNumbers)) {
    if (dataNumbers.length >= 2) {
      let max = Math.max.apply(null, dataNumbers); // mengambil nilai maksimum dari arrray

      // menampilkan nilai number yang sudah difilter yang nilai nya tidak sama dengan nilai dari variable max
      let filterDeleteAllMaxNumber = dataNumbers.filter((number) => number !== max);

      return Math.max.apply(null, filterDeleteAllMaxNumber); // mereturn nilai terbesar
    } else {
      return 'Jumlah array minimal ada 2 ';
    }
  } else {
    return 'Error : invalid data type';
  }
}

const dataAngka = [11, 10, 4, 7, 7, 4, 3, 2, 2, 8];

console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));

console.log(getAngkaTerbesarKedua());

// function getAngkaTerbesarKedua(dataNumbers) {
//   if (Array.isArray(dataNumbers)) {
//     if (dataNumbers.length >= 2) {
//       dataNumbers
//         .sort((i, j) => {
//           return i - j;
//         })
//         .reverse();

//       let angkaTerbesar = Math.max(...dataNumbers);

//       // Mengambil total angka terbesar
//       let banyakAngkaTerbesar = dataNumbers.filter((dataNumbers) => dataNumbers === angkaTerbesar).length;

//       let angkaTerbesarKedua = 0;

//       // untuk mengetahui letak angka terbesar kedua dari array data numbers
//       return dataNumbers[angkaTerbesarKedua + banyakAngkaTerbesar];
//     } else {
//       return 'Jumlah array minimal ada 2 ';
//     }
//   } else {
//     return 'Error : invalid data type';
//   }
// }
