const dataPenjualanNovel = [
  {
    idProduct: 'BOOK002421',
    namaProduk: 'Pulang - Pergi',
    penulis: 'Tere Liye',
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: 'BOOK002351',
    namaProduk: 'Selamat Tinggal',
    penulis: 'Tere Liye',
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Garis Waktu',
    penulis: 'Fiersa Besari',
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Laskar Pelangi',
    penulis: 'Andrea Hirata',
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

function sumSimilarKey(dataPenjualan) {
  let holder = {};
  dataPenjualan.forEach(function (dPenjualan) {
    if (holder.hasOwnProperty(dPenjualan.penulis)) {
      // menyimpan property penulis yang memiliki nama penulis yang sama, dan juga berisikan value total terjual dan disimpan ke object holder
      holder[dPenjualan.penulis] += dPenjualan.totalTerjual;
    } else {
      //menyimpan property penulis dengan berisi kan value total terjual ke object holder
      holder[dPenjualan.penulis] = dPenjualan.totalTerjual;
    }
  });
  let obj2 = [];
  for (let prop in holder) {
    // menyimpan ke dalam array, yang berisi object dengan property penulis dan total terjual
    obj2.push({ penulis: prop, totalTerjual: holder[prop] });
  }
  return obj2;
}

function getInfoPenjualan(dataPenjualan) {
  if (Array.isArray(dataPenjualan)) {
    let totalKeuntungan = 0;
    let totalModal = 0;

    for (let i = 0; i < dataPenjualan.length; i++) {
      if (typeof dataPenjualan[i] == 'object') {
        totalKeuntungan += (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli) * dataPenjualan[i].totalTerjual;
        totalModal += dataPenjualan[i].hargaBeli * (dataPenjualan[i].sisaStok + dataPenjualan[i].totalTerjual);
      } else {
        return 'Invalid data type';
      }
    }

    const totalTerjualBerdasarkanPenulis = sumSimilarKey(dataPenjualan);

    // mengambil nilai array yang memiliki property total terjual terbesar
    const maxPenulis = totalTerjualBerdasarkanPenulis.reduce(function (prev, cur) {
      return prev.totalTerjual > cur.totalTerjual ? prev : cur;
    });
    // mengambil nilai array yang memiliki property total terjual terbesar
    const maxBuku = dataPenjualan.reduce(function (prev, cur) {
      return prev.totalTerjual > cur.totalTerjual ? prev : cur;
    });

    return {
      totalKeuntungan: `Rp. ${totalKeuntungan.toLocaleString('id-ID')}`,
      totalModal: `Rp. ${totalModal.toLocaleString('id-ID')}`,
      persentaseKeuntungan: `${((totalKeuntungan / totalModal) * 100).toFixed(2)} %`,
      produkBukuTerlaris: maxBuku.namaProduk,
      penulisTerlaris: maxPenulis.penulis,
    };
  } else {
    return 'Error : invalid Data type';
  }
}

console.log(getInfoPenjualan(dataPenjualanNovel));
