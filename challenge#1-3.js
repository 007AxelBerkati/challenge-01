// const checkTypeNumber = (givenNumber) => {
//   if (typeof givenNumber === 'number') {
//     if (givenNumber % 2 === 0) {
//       return 'GENAP';
//     } else {
//       return 'GANJIL';
//     }
//   } else if (typeof givenNumber === 'string' || typeof givenNumber === 'object') {
//     return 'Error : invalid data type';
//   } else {
//     return 'Bro where is the parameter';
//   }
// };

function checkEmail(email) {
  const regex = /^([a-zA-Z0-9])(\.?[a-zA-Z0-9]){5,29}@([a-zA-Z]+)\.([a-zA-Z]{2,3})(\.[a-zA-Z]{2,3})?$/; // pola email
  if (regex.test(email) && typeof email === 'string') {
    return 'VALID';
  } else if (typeof email === 'undefined') {
    return 'ERROR : Tolong Isi Email Anda';
  } else {
    const reg = /(?=.*[@])/;
    if (!reg.test(email)) {
      return "ERROR : anda lupa menambahkan '@'";
    }
    return 'INVALID';
  }
}

console.log(checkEmail('apranata@binar.co.id'));
console.log(checkEmail('apranata@binar.com'));
console.log(checkEmail('apranata@binar'));
console.log(checkEmail('apranata')); // Error dikarenakan tidak ada symbol '@' setelah memberi nama pengguna
console.log(checkTypeNumber(checkEmail(3322))); // Error dikarenakan Function checkTypeNumber tidak ada

/*Error dikarenakan pemanggilan function tidak ada 
nilainya atau undefined, dimana seharusnya function yang dibuat perlu menerima nilai...
Disini untuk Error nya saya definisikan sendiri, 
karena pada javascript jika parameternya kita tidak isi, maka secara otomatis nilainya menjadi undefined */
console.log(checkEmail());
